/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

/**
 *
 * @author Christoffer Andersson
 */
public class DynamicArray<T>
{
    protected volatile Object[] array;

    public DynamicArray()
    {
        this.array = new Object[0];
    }

    public synchronized int add(T object)
    {
        synchronized(array)
        {
            for(int i = 0; i < array.length; i++)
            {
                if(array[i] == null)
                {
                    this.array[i] = object;
                    return i;
                }
            }
            this.expand();
            this.array[array.length - 1] = object;
            return array.length - 1;
        }
    }

    protected synchronized void expand()
    {
        synchronized(array)
        {
            Object[] objects = new Object[array.length + 1];
            for(int i = 0; i < array.length; i++)
            {
                objects[i] = array[i];
            }
            this.array = objects;
        }
    }

    protected synchronized void shorten(int steps)
    {
        synchronized(array)
        {
            Object[] objects = new Object[array.length - steps];
            for(int i = 0; i < objects.length; i++)
            {
                objects[i] = array[i];
            }
            this.array = objects;
        }
    }

    public synchronized void remove(T object)
    {
        synchronized(array)
        {
            for(int i = 0; i < array.length; i++)
            {
                if(array[i] == object)
                {
                    this.array[i] = null;
                    for(int j = array.length - 1; j >= 0 ; j--)
                    {
                        if(array[j] != null)
                        {
                            this.shorten(array.length - j - 1);
                        }
                    }
                    return;
                }
            }
        }
    }

    public synchronized void remove(int index)
    {
        synchronized(array)
        {
            this.array[index] = null;
            if(index == array.length - 1)
            {
                this.shorten(1);
            }
        }
    }

    public synchronized int indexOf(T object)
    {
        synchronized(array)
        {
            for(int i = 0; i < array.length; i++)
            {
                if(array[i] == object)
                {
                    return i;
                }
            }
            return -1;
        }
    }

    public synchronized boolean contains(T object)
    {
        return indexOf(object) > -1;
    }

    public void clear(int size)
    {
        this.array = new Object[0];
    }

    public T get(int index)
    {
        return (T)array[index];
    }

    public int getLength()
    {
        return array.length;
    }
}