/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

/**
 *
 * @author Christoffer Andersson
 */
public enum  ArrayType
{
    Int,
    Short,
    Float,
    Double,
    Long,
    Byte,
    Char,
    String,
    Boolean,
    Serializable,
    Null;

    public static int getIndex(ArrayType arrayType)
    {
        switch(arrayType)
        {
            case Int: return 0;
            case Short: return 1;
            case Float: return 2;
            case Double: return 3;
            case Long: return 4;
            case Byte: return 5;
            case Char: return 6;
            case String: return 7;
            case Boolean: return 8;
            case Serializable: return 9;
            default: return -1;
        }
    }

    public static ArrayType getType(int index)
    {
        switch(index)
        {
            case 0: return Int;
            case 1: return Short;
            case 2: return Float;
            case 3: return Double;
            case 4: return Long;
            case 5: return Byte;
            case 6: return Char;
            case 7: return String;
            case 8: return Boolean;
            case 9: return Serializable;
            default: return Null;
        }
    }
}