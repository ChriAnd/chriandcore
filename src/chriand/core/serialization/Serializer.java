/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

import chriand.core.Debugger;
import java.util.HashMap;

/**
 *
 * @author Christoffer Andersson
 */
public final class Serializer
{
    private static final HashMap<String, ISerializable> serializers = new HashMap<String, ISerializable>();

    public static void register(ISerializable serializable)
    {
        if(serializable == null)
        {
            return;
        }
        Serializer.serializers.put(serializable.getClassName(), serializable);
    }

    public static XMLNode serializeXML(ISerializable serializable, String tag)
    {
        XMLNode dataNode = new XMLNode(tag, serializable.getClassName());
        serializable.onSerialize(dataNode);
        return dataNode;
    }

    public static ISerializable deserializeXML(XMLNode xmlNode) throws SerializableTypeNotFoundException
    {
        ISerializable iSerializable;
        if("null".equals(xmlNode.getData()))
        {
            //.important(Serializer.class, "Found Tag <" + xmlNode.getTag() + "> With A Value Of [null]");
            return null;
        }
        try
        {
            iSerializable = serializers.get(xmlNode.getData()).clone(xmlNode);
        }
        catch(Exception e)
        {
            for(ISerializable i : serializers.values())
            {
                System.out.println(i.getClassName());
            }
            Debugger.error(Serializer.class, "The Given Type [" + xmlNode.getData() + "] Was Not Found In The Serializer Register");
            throw new SerializableTypeNotFoundException("The Given Type [" + xmlNode.getData() + "] Was Not Found In The Serializer Register");
        }
        iSerializable.onDeserialize(xmlNode);
        return iSerializable;
    }
    
    public static ISerializable createObject(String name)
    {
        return serializers.get(name).clone(null);
    }

    public static void init()
    {
        Serializer.register(new Array());
        Serializer.register(new List());
    }
}