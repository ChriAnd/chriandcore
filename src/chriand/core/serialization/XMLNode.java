/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

import java.awt.*;
import java.util.ArrayList;

/**
 *
 * @author Christoffer Andersson
 */
public class XMLNode
{
    protected String tag;
    protected String data;
    protected ArrayList<XMLNode> children;

    public XMLNode()
    {
        this.tag = "";
        this.data = "";
        this.children = new ArrayList<XMLNode>();
    }

    public XMLNode(String tag, String data)
    {
        this.tag = tag;
        this.data = data;
        this.children = new ArrayList<XMLNode>();
    }

    public XMLNode setTag(String tag)
    {
        this.tag = tag;
        return this;
    }

    public String getTag()
    {
        return tag;
    }

    public void setData(String data)
    {
        this.data = data;
    }

    public String getData()
    {
        return data;
    }

    public XMLNode add(Object data, String tag)
    {
        XMLNode node = null;
        if(data == null)
        {
            node = new XMLNode(tag, "null");
        }
        else if (data instanceof ISerializable)
        {
            node = Serializer.serializeXML((ISerializable) data, tag);
        }
        else if (data instanceof XMLNode)
        {
            node = (XMLNode)data;
        }
        else if (data.getClass().isEnum())
        {
            node = new XMLNode(tag, data.toString());
        }
        else if (data instanceof Color)
        {
            node = new XMLNode(tag, "Color");
            node.add(((Color)data).getRed(), "R");
            node.add(((Color)data).getGreen(), "G");
            node.add(((Color)data).getBlue(), "B");
            node.add(((Color)data).getAlpha(), "A");
        }
        else if (data instanceof javafx.scene.paint.Color)
        {
            node = new XMLNode(tag, "Color2");
            node.add(((javafx.scene.paint.Color)data).getRed(), "R");
            node.add(((javafx.scene.paint.Color)data).getGreen(), "G");
            node.add(((javafx.scene.paint.Color)data).getBlue(), "B");
            node.add(((javafx.scene.paint.Color)data).getOpacity(), "A");
        }
        else if (data instanceof Point)
        {
            node = new XMLNode(tag, "Point");
            node.add(((Point)data).x, "X");
            node.add(((Point)data).y, "Y");
        }
        else if (data instanceof Rectangle)
        {
            node = new XMLNode(tag, "Rectangle");
            node.add(((Rectangle)data).x, "X");
            node.add(((Rectangle)data).y, "Y");
            node.add(((Rectangle)data).width, "Width");
            node.add(((Rectangle)data).height, "Height");
        }
        else
        {
            node = new XMLNode(tag, data + "");
        }
        this.addChild(node);
        return this;
    }

    protected XMLNode get(String name) throws SerializableTypeNotFoundException
    {
        for(int i = children.size() - 1; i >= 0; i--)
        {
            if(children.get(i).tag.equals(name))
            {
                return children.get(i);
            }
        }
        System.out.println(toString());
        throw new SerializableTypeNotFoundException("The Given Type [" + name + "] Was Not Found");
    }

    public String getString(String name) throws SerializableTypeNotFoundException
    {
        return get(name).getData();
    }

    public char getChar(String name) throws SerializableTypeNotFoundException
    {
        return get(name).getData().charAt(0);
    }

    public boolean getBoolean(String name) throws SerializableTypeNotFoundException
    {
        return Boolean.parseBoolean(get(name).getData());
    }

    public byte getByte(String name) throws SerializableTypeNotFoundException
    {
        return Byte.parseByte(get(name).getData());
    }

    public int getInt(String name) throws SerializableTypeNotFoundException
    {
        return Integer.parseInt(get(name).getData());
    }

    public float getFloat(String name) throws SerializableTypeNotFoundException
    {
        return Float.parseFloat(get(name).getData());
    }

    public double getDouble(String name) throws SerializableTypeNotFoundException
    {
        return Double.parseDouble(get(name).getData());
    }

    public short getShort(String name) throws SerializableTypeNotFoundException
    {
        return Short.parseShort(get(name).getData());
    }

    public long getLong(String name) throws SerializableTypeNotFoundException
    {
        return Long.parseLong(get(name).getData());
    }

    public String getEnum(String name) throws SerializableTypeNotFoundException
    {
        return get(name).getData();
    }
    
    public XMLNode getNode(String name) throws SerializableTypeNotFoundException
    {
        return get(name);
    }

    public ISerializable getSerializable(String name) throws SerializableTypeNotFoundException
    {
        return Serializer.deserializeXML(get(name));
    }

    public Color getColor(String name) throws SerializableTypeNotFoundException
    {
        XMLNode node = get(name);
        return new Color(node.getInt("R"), node.getInt("G"), node.getInt("B"), node.getInt("A"));
    }
    
    public javafx.scene.paint.Color getColor2(String name) throws SerializableTypeNotFoundException
    {
        XMLNode node = get(name);
        return new javafx.scene.paint.Color(node.getDouble("R"), node.getDouble("G"), node.getDouble("B"), node.getDouble("A"));
    }

    public Point getPoint(String name) throws SerializableTypeNotFoundException
    {
        XMLNode node = get(name);
        return new Point(node.getInt("X"), node.getInt("Y"));
    }

    public Rectangle getRectangle(String name) throws SerializableTypeNotFoundException
    {
        XMLNode node = get(name);
        return new Rectangle(node.getInt("X"), node.getInt("Y"), node.getInt("Width"), node.getInt("Height"));
    }

    public void addChild(XMLNode node)
    {
        this.children.add(node);
    }

    public void removeChild(XMLNode node)
    {
        this.children.remove(node);
    }

    public static XMLNode fromString(String data)
    {
        XMLNode node = new XMLNode();
        node.fromText(new XMLHelper(data.replaceAll("^ | $|\\n ", "")), 0);
        return node;
    }

    private int fromText(XMLHelper xmlHelper, int start)
    {
        boolean collectTag = false;
        boolean collectValue = false;

        boolean hasReachedAnEndTag = false;
        boolean hasReachedStartOfMyStartTag = false;

        for (int i = start; i < xmlHelper.length(); i++)
        {
            //Has Found Start Of An Unknown End Tag
            if (xmlHelper.isStartOfEndTag(i))
            {
                collectValue = false;
                collectTag = false;
                hasReachedAnEndTag = true;
                i += 1;
                continue;
            }
            //Has Found Start Of My Start Tag
            else if (xmlHelper.isStartOfStartTag(i) && !hasReachedStartOfMyStartTag)
            {
                collectValue = false;
                collectTag = true;
                hasReachedStartOfMyStartTag = true;
                continue;
            }
            //Has Found Start Of A Comment's Tag
            else if (xmlHelper.isStartOfComment(i))
            {
                collectValue = false;
                collectTag = false;
                i += 3;
            }
            //Has Found Start Of A Child's Start Tag
            else if (xmlHelper.isStartOfStartTag(i))
            {
                collectValue = false;
                collectTag = false;
                XMLNode node = new XMLNode();
                i = node.fromText(xmlHelper, i);
                this.addChild(node);
                continue;
            }
            //Has Found End Of A Comment's Tag
            else if (xmlHelper.isEndOfComment(i))
            {
                collectValue = false;
                collectTag = false;
                i += 2;
            }
            //Has Found End Of My Start Tag
            else if (xmlHelper.isEndOfStartTag(i, tag) && collectTag)
            {
                collectValue = true;
                collectTag = false;
                continue;
            }
            //Has Found End Of My End Tag
            else if (xmlHelper.isEndOfEndTag(i, tag) && hasReachedAnEndTag)
            {
                collectValue = false;
                collectTag = false;
                i -= getSingleLineNode().length();
                xmlHelper.delete(i, getSingleLineNode().length());
                return i;
            }
            //Has Not Found Any Special Chars
            else
            {
                if (collectValue)
                {
                    this.data += xmlHelper.getChar(i);
                }
                else if (collectTag)
                {
                    this.tag += xmlHelper.getChar(i);
                }
            }
        }
        return 0;
    }

    public String toDataString()
    {
        String nodeText = getStartTag() + data;
        for (XMLNode node : children)
        {
            nodeText += node.toDataString();
        }
        nodeText += getEndTag();
        return nodeText;
    }

    @Override
    public String toString()
    {
        return toString(0, 4);
    }

    protected String toString(int currentSpace, int space)
    {
        String textSpace = "";
        for(int i = 0; i < currentSpace; i++)
        {
            textSpace += " ";
        }
        String nodeText = textSpace + getStartTag() + data;
        if(children.size() > 0)
        {
            nodeText += "\n";
            for(XMLNode node : children)
            {
                nodeText += node.toString(currentSpace + space, space);
            }
            nodeText += textSpace + getEndTag();
        }
        else
        {
            for(XMLNode node : children)
            {
                nodeText += node.toString(currentSpace + space, space);
            }
            nodeText += getEndTag();
        }
        nodeText += "\n";
        return nodeText;
    }

    protected String getStartTag()
    {
        return "<" + tag + ">";
    }

    protected String getEndTag()
    {
        return "</" + tag + ">";
    }

    public String getSingleLineNode()
    {
        return getStartTag() + data + getEndTag();
    }
}