/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Christoffer Andersson
 */
public class List implements ISerializable, Iterable<Object>
{
    protected volatile ArrayList<Object> objects;
    protected volatile ArrayType arrayType;

    public List()
    {

    }

    public List(ArrayType arrayType)
    {
        this.objects = new ArrayList<Object>();
        this.arrayType = arrayType;
    }
    
    public List(ArrayList arrayList, ArrayType arrayType)
    {
        this.objects = arrayList;
        this.arrayType = arrayType;
    }

    public synchronized void add(Object obj)
    {
        synchronized(objects)
        {
            this.objects.add(obj);
        }
    }

    public synchronized void remove(Object obj)
    {
        synchronized(objects)
        {
            this.objects.remove(obj);
        }
    }

    public synchronized void removeAt(int index)
    {
        synchronized(objects)
        {
            this.objects.remove(index);
        }
    }

    public synchronized void set(Object obj, int index)
    {
        synchronized(objects)
        {
            this.objects.set(index, obj);
        }
    }

    public synchronized Object get(int index)
    {
        synchronized(objects)
        {
            return objects.get(index);
        }
    }

    public synchronized <T> T getWithType(int index)
    {
        synchronized(objects)
        {
            return (T)objects.get(index);
        }
    }

    public synchronized ArrayList getList()
    {
        return objects;
    }

    public synchronized ArrayType getArrayType()
    {
        synchronized(arrayType)
        {
            return arrayType;
        }
    }

    public synchronized int getSize()
    {
        synchronized(objects)
        {
            return objects.size();
        }
    }

    public synchronized int indexOf(Object object)
    {
        synchronized(objects)
        {
            return objects.indexOf(object);
        }
    }

    public synchronized boolean contains(Object object)
    {
        synchronized(objects)
        {
            return objects.contains(object);
        }
    }

    @Override
    public void onSerialize(XMLNode xmlNode)
    {
        xmlNode.add(objects.size(), "Size");
        xmlNode.add(ArrayType.getIndex(arrayType), "ArrayType");
        for(int i = 0; i < objects.size(); i++)
        {
            xmlNode.add(objects.get(i), "Object" + i);
        }
    }

    @Override
    public void onDeserialize(XMLNode xmlNode) throws SerializableTypeNotFoundException
    {
        this.objects = new ArrayList<Object>();
        int size = xmlNode.getInt("Size");
        this.arrayType = ArrayType.getType(xmlNode.getInt("ArrayType"));

        if(arrayType == ArrayType.Byte)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getByte("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Boolean)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getBoolean("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Char)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getChar("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Double)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getDouble("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Float)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getFloat("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Int)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getInt("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Long)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getLong("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Serializable)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getSerializable("Object" + i));
            }
        }
        else if(arrayType == ArrayType.Short)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getShort("Object" + i));
            }
        }
        else if(arrayType == ArrayType.String)
        {
            for(int i = 0; i < size; i++)
            {
                this.objects.add(xmlNode.getString("Object" + i));
            }
        }
    }

    @Override
    public String getClassName()
    {
        return "List";
    }

    @Override
    public ISerializable clone(XMLNode xmlNode)
    {
        return new List();
    }

    @Override
    public Iterator<Object> iterator()
    {
        Iterator<Object> it = new Iterator<Object>()
        {
            private int currentIndex = 0;

            @Override
            public boolean hasNext()
            {
                return currentIndex < getSize() && objects.get(currentIndex) != null;
            }

            @Override
            public Object next()
            {
                return objects.get(currentIndex++);
            }

            @Override
            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }
}