/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

/**
 *
 * @author Christoffer Andersson
 */
public class XMLHelper
{
    private String text;

    public XMLHelper(String text)
    {
        this.text = text;
    }

    public void addText(String text)
    {
        this.text += text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getText()
    {
        return text;
    }

    public char getChar(int index)
    {
        return text.charAt(index);
    }

    public int length()
    {
        return text.length();
    }

    public boolean isStartOfStartTag(int i)
    {
        return getChar(i) == '<' && getChar(i + 1) != '/';
    }

    public boolean isEndOfStartTag(int i, String tag)
    {
        for (int j = 0; j < tag.length(); j++)
        {
            if (getChar(i - tag.length() + j) != tag.charAt(j))
            {
                return false;
            }
        }
        return getChar(i) == '>';
    }

    public boolean isStartOfEndTag(int i)
    {
        return getChar(i) == '<' && getChar(i + 1) == '/';
    }

    public boolean isEndOfEndTag(int i, String tag)
    {
        for (int j = 0; j < tag.length(); j++)
        {
            if (getChar(i - tag.length() + j) != tag.charAt(j))
            {
                return false;
            }
        }
        return getChar(i) == '>';
    }

    public boolean isStartOfComment(int i)
    {
        return getChar(i) == '<' &&getChar(i + 1) == '!' && getChar(i + 2) == '-' && getChar(i + 3) == '-';
    }

    public boolean isEndOfComment(int i)
    {
        return getChar(i) == '-' && getChar(i + 1) == '-' && getChar(i + 2) == '>';
    }

    public void delete(int start, int length)
    {
        if (start > 0)
        {
            String first = text.substring(0, start);
            String rest = text.substring(start + length);
            this.text = first + rest;
        }
    }
}