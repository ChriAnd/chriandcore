/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package chriand.core.serialization;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Christoffer Andersson
 */
public class Array implements ISerializable, Iterable<Object>
{
    protected volatile Object[] objects;
    protected volatile ArrayType arrayType;

    public Array()
    {
        
    }

    public Array(ArrayType arrayType, int size)
    {
        this.objects = new Object[size];
        this.arrayType = arrayType;
    }
    
    public Array(ArrayType arrayType, Object[] objects)
    {
        this.objects = objects;
        this.arrayType = arrayType;
    }
    
    public Array(ArrayType arrayType, ArrayList objects)
    {
        this.objects = new Object[objects.size()];
        this.objects = objects.<Object>toArray(this.objects);
        this.arrayType = arrayType;
    }

    public synchronized void set(Object obj, int index)
    {
        synchronized(objects)
        {
            this.objects[index] = obj;
        }
    }

    public synchronized Object get(int index)
    {
        synchronized(objects)
        {
            return objects[index];
        }
    }

    public synchronized <T> T getWithType(int index)
    {
        synchronized(objects)
        {
            return (T)objects[index];
        }
    }

    public synchronized ArrayType getArrayType()
    {
        synchronized(arrayType)
        {
            return arrayType;
        }
    }

    public synchronized int getSize()
    {
        synchronized(objects)
        {
            return objects.length;
        }
    }
    
    public synchronized Object[] toArray()
    {
        return objects;
    }

    @Override
    public void onSerialize(XMLNode xmlNode)
    {
        xmlNode.add(objects.length, "Size");
        xmlNode.add(ArrayType.getIndex(arrayType), "ArrayType");
        for(int i = 0; i < objects.length; i++)
        {
            xmlNode.add(objects[i], "Object" + i);
        }
    }

    @Override
    public void onDeserialize(XMLNode xmlNode) throws SerializableTypeNotFoundException
    {
        this.objects = new Object[xmlNode.getInt("Size")];
        this.arrayType = ArrayType.getType(xmlNode.getInt("ArrayType"));

        if(arrayType == ArrayType.Byte)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getByte("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Boolean)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getBoolean("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Char)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getChar("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Double)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getDouble("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Float)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getFloat("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Int)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getInt("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Long)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getLong("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Serializable)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getSerializable("Object" + i);
            }
        }
        else if(arrayType == ArrayType.Short)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getShort("Object" + i);
            }
        }
        else if(arrayType == ArrayType.String)
        {
            for(int i = 0; i < objects.length; i++)
            {
                this.objects[i] = xmlNode.getString("Object" + i);
            }
        }
    }

    @Override
    public String getClassName()
    {
        return "Array";
    }

    @Override
    public ISerializable clone(XMLNode xmlNode)
    {
        return new Array();
    }
    
    @Override
    public Iterator<Object> iterator()
    {
        Iterator<Object> it = new Iterator<Object>()
        {
            private int currentIndex = 0;

            @Override
            public boolean hasNext()
            {
                return currentIndex < getSize() && objects[currentIndex] != null;
            }

            @Override
            public Object next()
            {
                return objects[currentIndex++];
            }

            @Override
            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }
}