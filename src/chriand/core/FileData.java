/*
 * Copyright (c) 2015, Christoffer Andersson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package chriand.core;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 *
 * @author Christoffer Andersson
 */
public class FileData
{
    protected byte[] bytes;

    public FileData(byte[] bytes)
    {
        this.bytes = bytes;
    }

    public FileData(File file) throws IOException
    {
        if(file.isFile())
        {
            byte[] fileName = file.getPath().substring(file.getPath().indexOf(File.separator)).getBytes();
            ByteBuffer byteBuffer = ByteBuffer.allocate((int) (file.length() + 4 + fileName.length));
            byteBuffer.putInt(fileName.length);
            byteBuffer.put(fileName);
            this.bytes = byteBuffer.array();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(bytes, fileName.length + 4, (int) file.length());
            bufferedInputStream.close();
        }
    }

    public byte[] getBytes()
    {
        return bytes;
    }

    public void save(String path) throws IOException
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        byte[] fileName = new byte[byteBuffer.getInt()];
        byteBuffer.get(fileName);
        File file = new File(path + File.separator + new String(fileName).replaceAll("[/\\\\]+", Matcher.quoteReplacement(System.getProperty("file.separator"))));
        File pathFile = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - file.getName().length()));
        pathFile.mkdirs();
        file.createNewFile();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file.getAbsolutePath()));
        bufferedOutputStream.write(bytes, 4 + fileName.length, bytes.length - 4 - fileName.length);
        bufferedOutputStream.close();
    }
    
    public static void toFolder(FileData[] fileDatas, String path) throws IOException
    {
        for(FileData fileData : fileDatas)
        {
            fileData.save(path);
        }
    }
    
    public static FileData[] fromFolder(File folder) throws IOException
    {
        if(folder.isDirectory())
        {
            ArrayList<File> files = getFiles(folder);
            FileData[] fileDatas = new FileData[files.size()];
            for(int i = 0; i < files.size(); i++)
            {
                fileDatas[i] = new FileData(files.get(i));
            }
            return fileDatas;
        }
        return null;
    }
    
    private static ArrayList<File> getFiles(File folder)
    {
        ArrayList<File> allFiles = new ArrayList<File>();
        File[] files = folder.listFiles();
        for(int i = 0; i < files.length; i++)
        {
            if(!files[i].isDirectory())
            {
                allFiles.add(files[i]);
            }
            else
            {
                allFiles.addAll(getFiles(files[i]));
            }
        }
        return allFiles;
    }
}