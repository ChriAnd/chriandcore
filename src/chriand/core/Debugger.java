/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chriand.core;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Christoffer
 */
public final class Debugger
{
    public static final String COLOR_RESET = "\u001B[0m";
    public static final String COLOR_BLACK = "\u001B[30m";
    public static final String COLOR_RED = "\u001B[31m";
    public static final String COLOR_GREEN = "\u001B[32m";
    public static final String COLOR_YELLOW = "\u001B[33m";
    public static final String COLOR_BLUE = "\u001B[34m";
    public static final String COLOR_PURPLE = "\u001B[35m";
    public static final String COLOR_CYAN = "\u001B[36m";
    public static final String COLOR_WHITE = "\u001B[37m";
    private static boolean ready;

    public static synchronized void log(Object caller, String text)
    {
        synchronized(System.out)
        {
            System.out.println(COLOR_RESET + "[" + getDate() + "] - [" + getCaller(caller) + "] - " + text);
        }
    }

    public static synchronized void important(Object caller, String text)
    {
        synchronized(System.out)
        {
            System.out.println("[" + getDate() + "] - [" + COLOR_YELLOW + getCaller(caller) + COLOR_RESET + "] - " + text);
        }
    }

    public static synchronized void success(Object caller, String text)
    {
        synchronized(System.out)
        {
            System.out.println("[" + getDate() + "] - [" + COLOR_GREEN + getCaller(caller) + COLOR_RESET + "] - " + text);
        }
    }
    
    public static synchronized void network(Object caller, String text)
    {
        synchronized(System.out)
        {
            System.out.println("[" + getDate() + "] - [" + COLOR_BLUE + getCaller(caller) + COLOR_RESET + "] - " + text);
        }
    }

    public static synchronized void error(Object caller, String text)
    {
        synchronized(System.out)
        {
            System.out.println("[" + getDate() + "] - [" + COLOR_RED + getCaller(caller) + COLOR_RESET + "] - " + text);
        }
    }

    public static synchronized void error(Object caller, Exception e)
    {
        synchronized(System.out)
        {
            System.out.println("[" + getDate() + "] - [" + COLOR_RED + getCaller(caller) + COLOR_RESET + "] - " + "Critical error occurred");
            e.printStackTrace();
        }
    }

    private static String getDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SS");
        return sdf.format(calendar.getTime());
    }

    private static String getCaller(Object caller)
    {
        if(caller instanceof Class)
        {
            return ((Class)caller).getSimpleName();
        }
        return caller.getClass().getSimpleName();
    }
}